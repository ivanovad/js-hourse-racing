const horseRaceGame = new HorseRacing();

function HorseRacing() {
  this.bank = 1000;
  this.horses = [
    new Horse("Август"),
    new Horse("Ястреб"),
    new Horse("Гарольд"),
    new Horse("Бандит"),
    new Horse("Юпитер"),
  ];
  this.player = new Player(this.bank);
  this.wagers = [];
  this.clearWagers = function () {
    this.wagers.length = 0;
  };

  function Horse(name) {
    this.name = name;
    this.run = function () {
      return new Promise((resolve) => {
        const racingTime = getRandomInt(500, 3000);
        const horseRace = {
          name: this.name,
          racingTime,
        };
        setTimeout(() => {
          resolve(horseRace);
        }, racingTime);
      });
    };
  }

  function Player(bank) {
    this.account = bank;
  }

  function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  }

  this.isCorrectWager = function (sumToBet) {
    return sumToBet > 0 && typeof sumToBet === "number";
  };

  this.isEnoughMoney = function (sumToBet) {
    return sumToBet <= this.player.account;
  };

  this.takeMoney = function (sumToTake) {
    this.player.account -= sumToTake;
  };

  this.checkPlayerWin = function (winnerHorse) {
    //перебираем через filter на случай, если игрок сделал несколько мелких ставок
    //на одну и ту же лошадь
    let winningWagers = this.wagers.filter(
      (wager) => wager.name === winnerHorse
    );

    if (winningWagers.length === 0) {
      console.log("Вы ничего не выиграли");
      console.log(`Ваш счёт ${this.player.account} денег`);
    } else {
      let wonMoney = 0;

      winningWagers.forEach((wager) => {
        wonMoney += wager.wagerSum * 2;
      });

      this.player.account += wonMoney;

      console.log(`Ваша ставка сыграла! Вы выиграли ${wonMoney} денег!`);
      console.log(`Теперь ваш счёт ${this.player.account} денег!`);
    }

    this.clearWagers();
  };
}

function showHorses() {
  horseRaceGame.horses.forEach((horse) => {
    console.log(horse.name);
  });
}

function showAccount() {
  console.log(horseRaceGame.player.account);
}

function setWager(name, sumToBet) {
  if (horseRaceGame.isCorrectWager(sumToBet)) {
    if (horseRaceGame.isEnoughMoney(sumToBet)) {
      horseRaceGame.wagers.push({ name, wagerSum: sumToBet });
      horseRaceGame.takeMoney(sumToBet);
    } else {
      console.log("На счету недостаточно средств");
    }
  } else {
    console.log("Некорректная сумма ставки");
  }
}

function startRacing() {
  if (horseRaceGame.wagers.length === 0) {
    console.log("Вы не сделали ставку");
  } else {
    let horsesPromises = horseRaceGame.horses.map((horse) => horse.run());

    let winnerHorseName;
    Promise.race(horsesPromises).then((winnerHorse) => {
      console.log("Победитель:");
      winnerHorseName = winnerHorse.name;
      console.log(winnerHorseName, winnerHorse.racingTime);
    });

    Promise.all(horsesPromises).then((racingResults) => {
      console.log("Результаты всех лошадей:");
      racingResults.forEach((horse) => {
        console.log(horse.name, horse.racingTime);
      });
      horseRaceGame.checkPlayerWin(winnerHorseName);
    });
  }
}

function newGame() {
  horseRaceGame.player.account = horseRaceGame.bank;
  horseRaceGame.clearWagers();
}
